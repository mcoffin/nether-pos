#!/usr/bin/python

# nether-pos.py Nether position calculator for Minecraft
# Copyright (C) 2019  Matt Coffin
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import argparse

scale_factor = 8

class Base:
    def __init__(self, overworld, nether):
        self.overworld = overworld
        self.nether = nether

    def relative_base(self, other_overworld):
        positions = zip(self.overworld, other_overworld)
        deltas = map(lambda a: round((a[1] - a[0]) / scale_factor), positions)
        nether_positions = map(lambda v: v[0] + v[1], zip(self.nether, deltas))
        return Base(other_overworld, list(nether_positions))

def main():
    parser = argparse.ArgumentParser(description = 'Nether position calculator')
    parser.add_argument('--base', type = int, required = True, nargs = 6)
    parser.add_argument('--target', type = int, required = True, nargs = 3)
    args = parser.parse_args()
    print(args)
    base_overworld = args.base[0:3]
    base_nether = args.base[3:6]
    base = Base(base_overworld, base_nether)
    print('{} {}'.format(base.overworld, base.nether))
    base_prime = base.relative_base(args.target)
    print('{} {}'.format(base_prime.overworld, base_prime.nether))

if __name__ == '__main__':
    main()
