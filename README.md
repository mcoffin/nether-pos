# nether-pos

`nether-pos` is a simple nether position calculator for minecraft

# Usage

```
nether.pos.py --base X Y Z NETHER_X NETHER_Y NETHER_Z --target NEW_X NEW_Y NEW_Z
```
